What is OpenEDU?
********************************************************************************
OpenEDU is a flexible, feature-rich content management framework built with
Drupal and specifically tailored for Higher Education institutions. OpenEDU
provides robust publishing features, customizable design layouts, rapid site
deployment capabilities and powerful content syndication and sharing
functionality, offering significant efficiencies and cost-savings across your
school’s web platform.


Where can I find out more on this amazing thing!
********************************************************************************
http://imagexmedia.com/openedu/


What do I get in this module?
********************************************************************************
A block, or bean to be more exact, that allows you to create list of links.
After installation go to admin/content/blocks or block/add

